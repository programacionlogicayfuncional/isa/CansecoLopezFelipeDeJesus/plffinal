(ns examen.core
  (:gen-class))

(defn arriba
  [x]
  (count (apply vector (filter #(= % \^) (apply vector x)))))

(defn derecha
  [x]
  (count (apply vector (filter #(= % \>) (apply vector x)))))

(defn izquierda
  [x]
  (count (apply vector (filter #(= % \<) (apply vector x)))))

(defn abajo
  [x]
  (count (apply vector (filter #(= % \v) (apply vector x)))))

(defn contarPasos
  [x]
  (+ (+ (arriba x) (derecha x)) 
     (+ (izquierda x) (abajo x))))

(defn resta [x] 
  (
   - (+ (arriba x) (derecha x)) (+ (abajo x) (izquierda x))
  ))

(defn problema1
  [x]
  (cond
    (== (contarPasos x) (count (apply vector x))) (resta x)
    :else 10))

(defn regresa-al-punto-de-origen? 
  [x]
  (zero? (problema1 x)))
  
(regresa-al-punto-de-origen? "") 
(regresa-al-punto-de-origen? [])
(regresa-al-punto-de-origen? (list))


(regresa-al-punto-de-origen? "><")
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? "<<vv>>^^")
